import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { BreakoutComponent } from "../breakout/breakout.component";
import { Router } from "@angular/router";
import { localService } from "src/app/services/data.service";

@Component({
  selector: "app-meeting-room",
  templateUrl: "./meeting-room.component.html",
  styleUrls: ["./meeting-room.component.scss"],
})
export class MeetingRoomComponent implements OnInit {
  // @Input() a:any;

  constructor(public router: Router,private _ls:localService) {}
  // @ViewChild(BreakoutComponent) 'xyz': BreakoutComponent;

  ngOnInit(): void {}

  gotoMeeting(a) {
    // alert(a)
    // this.xyz.gotoRoom(a);
    if (a == "meeting1") {
      this._ls.stepUpAnalytics('click_meeting1');
      this.router.navigate(["/breakout"]);
      
    }
    if (a == "meeting2") {
      this._ls.stepUpAnalytics('click_meeting2');
      this.router.navigate(["/breakout2"]);
    }
    if (a == "meeting3") {
      this._ls.stepUpAnalytics('click_meeting3');
      this.router.navigate(["/breakout3"]);
    }
    if (a == "meeting4") {
      this._ls.stepUpAnalytics('click_meeting4');
      this.router.navigate(["/breakout4"]);
    }
  }
}
