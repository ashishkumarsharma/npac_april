import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditoriumRoutingModule } from './auditorium-routing.module';
import { AuditoriumComponent } from './auditorium.component';

@NgModule({
  declarations: [AuditoriumComponent],
  imports: [
    CommonModule,
    AuditoriumRoutingModule
  ]
})
export class AuditoriumModule { }
